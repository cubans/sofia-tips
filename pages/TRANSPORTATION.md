# Transporte en Sofia
 
 El transporte público de la ciudad es una maravilla comparado con la coyuntura transportista cubana. Comienza alrededor de las **4am** y termina poco antes de las **12am**, así que si te vas de *perdularias* (*ver Glosario de Terminos Cubano-B(V)ulgaros*) ten en cuenta que después de las 12 no existe la confronta, solo unos ómnibus nocturnos y los taxis, de los cuales hablaremos luego. Empecemos con lo básico:

 ## Tickets y Tarjetas
 En reusmen, el ticket para el transporte durante el dia tiene un precio de **1.60 BGN**, tanto para el metro como para los diferentes transportes sobre tierra (bus, trams y trolleybus) pero, a no ser que seas de los que nunca sale de casa, hay opciones mas economicas y practicas. 
 Si mayormente coges el metro y haces poco uso de los demas medios de transporte, puedes comprar alguna de las tarjetas de metro. Si te mueves poco, la de 10 viajes puede que sea suficiente. Tiene un costo de **12 BGN** (1.2 BGN por viaje) y la puedes adquirir y recargar en casi todas las estaciones de metro. Si viajas mas, entonces las mensuales pueden ser una mejor opcion. Van desde 35 BGN las personalizadas hasta 42 BGN las no personalizadas y puedes usarla cuantas veces desees en el Metro durante el periodo de un mes a partir de sacada. Por supuesto, tambien puedes recargarlas. 
 Ahora, estas tarjetas anteriores solo sirven para Metro. Tendras que seguir pagando los demas medios de transporte. Por eso, los cubanos optamos por las tarjetas generales. Son ligeramente mas caras, pero cubren todo el transporte publico, excepto los omnibus nocturnos y los rurales. Estas tarjetas pueden ser personalzadas o no, y dependiendo de esto varia el precio. Al valor de que ves en la tabla siguiente, tienes que agregarle la primera vez **2 BGN** por la tarjeta plastica, y si no tienes una foto de carnet clasica contigo, **8 BGN** mas por el servicio de fotografia.

| Tiempo   | Precio (Pers.)|
|----------|--------------:|
| 1 mes    |     50.00     |
| 3 meses  |    130.00     |
| 6 meses  |    255.00     |
| 1 аño    |    365.00     |

Puedes ver otras posibles tarjetas y mas detalles en este [link](https://www.metrosofia.com/en/tickets) y para sacarlas, aqui puedes encontrar una [tabla](https://www.sofiatraffic.bg/en/transport/punktove-za-prodazhba-na-prevozni/p/1) con las direcciones. Ten en cuenta que aunque en todos se puede recargar, solo en algunos de ellos la emiten. Ademas, intenta siempre ir con cash, para evitar POS rotos, que aunque nunca hemos pasado por eso, la coyuntura nos persigue y nunca se sabe. Ah, y **no puede faltar tu pasaporte** para sacarla.

## Metro

El Metro es el metodo mas rapido y facil para moverse de un lado al otro de la ciudad. Actualmente hay dos lineas:

![Mapa del Metro](https://www.metrosofia.com/images/sofia-metro-lines-2016-july.png)

1. Business Park - Silvnitsa
2. Sofia Airport - Vitosha

Actualmente se estra construyendo una tercera linea del metro que atraviesa las lineas actuales, y debe estar terminada antes de mediados de 2020. Si queires saber mas visita este [link](https://www.metropolitan.bg/en/op-transport-extension/istoriya-razvitie-metro/line-3-sofia-metro)

Entre Slivnitsa y Mladost 1 ambas lineas coinciden, por lo que hay una mayor frecuencia de trenes, que suele ser de 3 a 7 minutos; mientras en el resto de los tramos va de 7 a 14 minutos dependiendo de la hora. Echale un ojo a esta [tabla](https://www.metrosofia.com/bg/schedule) para que sepas mejor.

Como hablamos en la seccion de los tickets y tarjetas, lo mejor suele ser tener las tarjetas de limites temporales, pero los tickets los puedes comprar en cada terminal, tanto en las cajas como en unas maquinas si tienes el menudo.

<pre>
El ticket del metro solo puede ser usado para viajar hasta <b>3 minutos</b> despues de emitido
</pre>

### Por donde salir

Uno de los grandes problemas con el metro son las salidas. Casi todas las estaciones tienen 4 salidas, 2 en cada extremo del anden. Trata de siempre tener claro hacia donde vas, porque si no puede darse el caso en algunas que tienes que volver a entrar al anden (pagar o gastar ticket si no tienes tarjeta) para pasar al otro lado, pues muchas veces no hay pasos peatonales en las avenidas en un largo tramo y la estacion del metro se vuelve la unica via para cruzar esas calles. Puedes hacer uso de Google Maps para que te guie.

### Tiendas

Todas las estaciones de metro tienen tiendas pequenas donde puedes comprar pizza, dulces, snacks, revisas (si sabes bulgaro) y suele haber hasta una farmacia, por lo que son un lugar barato para merendar o desayunar sin perder mucho tiempo.


## Bus, Trolleybus y Trams

TODO

## Buses Nocturnos

TODO

## Taxis

TODO

## Transporte Ecologico y Alternativo

TODO

## Apps importantes

Para ver las apps utiles para moverte por la ciudad, ve a la seccion de [Apps y Sitios web de utilidad](./APPSANDWEB.md)


## Tips y guajiradas a evitar

* Ten en cuenta que al usar una tarjeta de tiempo limitado en el metro, si te equivocaste de linea, no podras volver a usarla en ese momento y tendras qeu pagar el ticket. Esto es para evitar el uso de una tarjeta por varias personas a la vez.
* Estate al tanto de cuando se te vence la tarjeta, o te podras encontrar una manhana sin poder entrar al metro y sin menudo o cash.
* Para evitar estos problemas, puedes comprar tambien la tarjeta de 10 viajes, y en caso de quedarte sin tiempo en la tarjeta general, usarlo o recargarlo ahi mismo en la estacion.
* Aunque puedes subir al bus sin pagar, puedes tener la mala suerte de algunos de encontrarte con un inspector, que carinhosamente te pondra una multa de 40 BGN
* No olvides apretar el boton de __*open the fucking doors*__ en los omnibus y trolleybus a la hora de subir y bajar, o puede que te quedes sin subir o de pases de parada. Aqui no se vale un **CHOFE ESPERATEEEEE!!**
* No existen chances, asi que si te pasaste de parada o tienes que caminar, camina.
* Las tarjetas se usan por NFC, por lo que si tienes una tarjeta de banco u otra pegada a ella en la billetera y pasas la billetera, no te la reconocera
* En las terminales de metro suelen haber agentes que si notan que eres extranjero te pediran identificacion. Anda siempre con tu *blue card* o tu pasasporte si aun no la tienes.
